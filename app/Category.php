<?php
namespace DibiWorkshop;

class Category extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'categories';
    protected $fillable = ['name', 'slug'];

    public function events()
    {
        return $this->hasMany('\\DibiWorkshop\\Event', '');
    }

    public function upcomingEvents()
    {
        return Event::where('starts', '>', date('Y-m-d H:i:s'))->get();
    }
}
