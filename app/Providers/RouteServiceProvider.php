<?php namespace DibiWorkshop\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider {

	/**
	 * All of the application's route middleware keys.
	 *
	 * @var array
	 */
	protected $middleware = [
		'auth' => 'DibiWorkshop\Http\Middleware\Authenticated',
		'auth.basic' => 'DibiWorkshop\Http\Middleware\AuthenticatedWithBasicAuth',
		'csrf' => 'DibiWorkshop\Http\Middleware\CsrfTokenIsValid',
		'guest' => 'DibiWorkshop\Http\Middleware\IsGuest',
	];

	/**
	 * Called before routes are registered.
	 *
	 * Register any model bindings or pattern based filters.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function before(Router $router)
	{
		$router->bind('categories', function ($value) {
		    try {
		    	return \DibiWorkshop\Category::where('slug', $value)->firstOrFail();
		    } catch (\Exception $e) {
		    	throw new \Symfony\Component\HttpKernel\Exception\HttpException(404);
		    }
		});
	}

	/**
	 * Define the routes for the application.
	 *
	 * @param  \Illuminate\Routing\Router  $router
	 * @return void
	 */
	public function map(Router $router)
	{
		$router->group(['namespace' => 'DibiWorkshop\Http\Controllers'], function($router)
		{
			require app_path('Http/routes.php');
		});
	}

}
