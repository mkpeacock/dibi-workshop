<?php
namespace DibiWorkshop;

class Event extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'events';
    protected $fillable = ['name', 'slug', 'starts', 'ends', 'category_id'];

    public function category()
    {
        return $this->hasOne('\\DibiWorkshop\\Category', 'category_id');
    }
}
