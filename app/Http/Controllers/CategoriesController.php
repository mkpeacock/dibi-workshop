<?php namespace DibiWorkshop\Http\Controllers;

use DibiWorkshop\Http\Controllers\Controller;

class CategoriesController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$categories = \DibiWorkshop\Category::paginate(1);

        return view('categories.index', compact('categories'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('categories.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(\DibiWorkshop\Http\Requests\CreateCategoryRequest $request)
	{
		// Note: causes mass assignment error unless...
		// ...model has fillable or guarded property
		// so that needs to be discussed

		$category = \DibiWorkshop\Category::create([
			'name' => $request->get('name'),
			'slug' => $request->get('slug')
		]);

		return redirect('/categories');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(\DibiWorkshop\Category $category)
	{
		return view('categories.view', compact('category'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit(\DibiWorkshop\Category $category)
	{
		//dd($category);
        return view('categories.edit', compact('category'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(\DibiWorkshop\Http\Requests\EditCategoryRequest $request, \DibiWorkshop\Category $category)
	{
		dd($category);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(\DibiWorkshop\Category $category)
	{
		$category->delete();

		return redirect('/categories');
	}

}
