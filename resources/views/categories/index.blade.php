@extends('layouts/default')

@section('content')
    <h1>Categories</h1>

    @if ($categories->count() == 0)
        <p>No categories</p>
    @else
        @foreach ($categories as $category)
            <li>{!! link_to("/categories/{$category->slug}", $category->name) !!}</li>
        @endforeach

        {!! $categories->render() !!}
    @endif
@stop
