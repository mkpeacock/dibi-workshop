@extends('layouts/default')

@section('content')
    <h1>Create a new category</h1>

    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach

    {!! Form::open(['route' => ['categories.store']]) !!}
    {{-- Form::open(['route' => ['tasks.store', 5]]) --}}

        <div class="form-group">
            {!! Form::label('task', 'Category name:') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('task', 'URL Slug:') !!}
            {!! Form::text('slug', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
        {!! Form::submit('Create category', ['class' => 'btn btn-primary form-control']) !!}
        </div>

    {!! Form::close() !!}
@stop
