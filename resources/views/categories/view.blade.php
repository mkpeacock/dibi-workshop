@extends('layouts/default')

@section('content')
    <h1>Category</h1>

    <p>{{ $category->name }}</p>

    @foreach ($category->events as $event)
        <li>{{ $event->name }}</li>
    @endforeach

    @foreach ($category->upcomingEvents() as $event)
        <li>{{ $event->name }}</li>
    @endforeach
@stop
