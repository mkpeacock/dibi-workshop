@extends('layouts/default')

@section('content')
    <h1>Edit category</h1>

    @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach

    {!! Form::model($category, ['route' => ['categories.update', $category->slug], 'method' => 'patch']) !!}

        <div class="form-group">
            {!! Form::label('name', 'Category') !!}
            {!! Form::text('name', $category->name, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('slug', 'Slug') !!}
            {!! Form::text('slug', $category->slug, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
        {!! Form::submit('save category', ['class' => 'btn btn-primary form-control']) !!}
        </div>

    {!! Form::close() !!}

    {!! Form::model($category, ['route' => ['categories.destroy', $category->slug], 'method' => 'delete']) !!}

        <div class="form-group">
        {!! Form::submit('delete category', ['class' => 'btn btn-primary form-control']) !!}
        </div>

    {!! Form::close() !!}
@stop
